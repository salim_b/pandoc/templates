# Pandoc templates

A collection of custom [Pandoc templates](https://pandoc.org/MANUAL.html#templates), regularly adapted to the latest Pandoc version.
